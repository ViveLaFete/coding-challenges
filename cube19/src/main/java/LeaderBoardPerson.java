public final class LeaderBoardPerson {

    private String name;
    private int score;

    private LeaderBoardPerson(){}

    public static LeaderBoardPerson getInstance(String name, int score) {
        LeaderBoardPerson p = new LeaderBoardPerson();
        p.score = score;
        p.name = name;
        return p;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

}
