final class HighestScore {

    private final int highestScore;
    private final int playAchievedOn;

    private HighestScore(int highestScore, int playAchievedOn){
        this.highestScore = highestScore;
        this.playAchievedOn = playAchievedOn;
    }

    public static HighestScore getInstance(int score, int playAchievedOn) {
        return new HighestScore(score, playAchievedOn);
    }

    public int getHighestScore() {
        return highestScore;
    }

    public int getPlayAchievedOn() {
        return playAchievedOn;
    }
}
