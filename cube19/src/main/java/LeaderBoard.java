import java.util.*;

final class LeaderBoard {

    static List<LeaderBoardPerson> getUpdatedLeaderBoard(final LeaderBoardPerson person, final List<LeaderBoardPerson> oldLeaderBoard) {
        List<LeaderBoardPerson> currentLeaderBoard = new ArrayList<>(oldLeaderBoard);
        final int position = findPositionOfPerson(person.getName(), currentLeaderBoard);
        boolean isNewEnry = false;
        if (position == currentLeaderBoard.size()) {
            //person not in leader board
            currentLeaderBoard.add(person);
            isNewEnry = true;
        } else {
            currentLeaderBoard.set(position, LeaderBoardPerson.getInstance(person.getName(), person.getScore()));
        }
        currentLeaderBoard.sort(Comparator.comparingInt(LeaderBoardPerson::getScore).reversed());
        final int positionAfterEntry = findPositionOfPerson(person.getName(), currentLeaderBoard);
        printOutputOneBased(person, position, isNewEnry, positionAfterEntry);
        return currentLeaderBoard;
    }

    static void prettyPrintLeaderBoard(int currentPlay, List<LeaderBoardPerson> leaderBoardPeople) {
        int rank = 1;
        for (LeaderBoardPerson person : leaderBoardPeople) {
            System.out.println(rank + " - " + person.getName() + " " + person.getScore());
            rank++;
        }
        System.out.println("Current Play - " + currentPlay);
    }

    static private int findPositionOfPerson(String name, List<LeaderBoardPerson> leaderBoard) {
        int count = 0;
        for (LeaderBoardPerson person : leaderBoard) {
            if (person.getName().equals(name)) {
                return count;
            }
            count++;
        }
        return count;
    }

    private static void printOutputOneBased(LeaderBoardPerson person, int position, boolean isNewEnry, int positionAfterEntry) {
        position++;
        positionAfterEntry++;
        if (isNewEnry) {
            System.out.println("Person " + person.getName() + " enters the table at position " + positionAfterEntry);
        } else {
            if (positionAfterEntry < position) {
                System.out.println("Person " + person.getName() + " climbs from position " + position + " to " + positionAfterEntry);
            } else if (positionAfterEntry == position) {
                System.out.println("Person " + person.getName() + " maintains position " + position);
            } else {
                System.out.println("Person " + person.getName() + " falls from position " + position + " to " + positionAfterEntry);
            }
        }
    }

}
