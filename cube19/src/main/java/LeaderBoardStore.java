import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;

public final class LeaderBoardStore {

    private final Deque<List<LeaderBoardPerson>> leaderBoards = new ConcurrentLinkedDeque<>();

    public void makePlay(LeaderBoardPerson person) {
        List<LeaderBoardPerson> mostRecent = new ArrayList<>(1);
        if (!leaderBoards.isEmpty()) {
            mostRecent = leaderBoards.getLast();
        }
        List<LeaderBoardPerson> currentLeaderBoard = LeaderBoard.getUpdatedLeaderBoard(person, mostRecent);
        leaderBoards.offer(currentLeaderBoard);
    }

    public Deque<List<LeaderBoardPerson>> getLeaderBoards() {
        return leaderBoards;
    }

    public HighestScore getHighestScoreEverAchievedByPerson(String name) {
        HighestScore highestScoreSoFar = HighestScore.getInstance(0, 0);
        int play = 0;
        for (List<LeaderBoardPerson> leaderBoard : leaderBoards) {
            for (LeaderBoardPerson person : leaderBoard) {
                if (person.getName().equals(name)) {
                    int scoreOnThisPlay = person.getScore();
                    if (scoreOnThisPlay > highestScoreSoFar.getHighestScore()) {
                        highestScoreSoFar = HighestScore.getInstance(scoreOnThisPlay, play);
                    }
                }
            }
            play++;
        }
        return highestScoreSoFar;
    }
}
