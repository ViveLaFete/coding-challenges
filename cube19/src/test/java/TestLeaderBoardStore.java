import org.junit.Assert;
import org.junit.Test;

public class TestLeaderBoardStore {

    @Test
    public void testHighestScore() {
        LeaderBoardStore store = new LeaderBoardStore();

        LeaderBoardPerson person = LeaderBoardPerson.getInstance("Nick", 10);
        store.makePlay(person);
        final int higherScore = 34;
        LeaderBoardPerson person1 = LeaderBoardPerson.getInstance("Nick", higherScore);
        store.makePlay(person1);
        LeaderBoardPerson person2 = LeaderBoardPerson.getInstance("Nick", 2);
        store.makePlay(person2);

        HighestScore hs = store.getHighestScoreEverAchievedByPerson("Nick");
        Assert.assertEquals(34, hs.getHighestScore());
    }

    //test historical leaderboard

}
