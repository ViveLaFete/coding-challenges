import org.junit.Assert;
import org.junit.Test;

public class TestAddPerson {

    @Test
    public void testAddPerson() throws Exception {
        String userInput = "score Nick 10";
        LeaderBoardPerson person = App.parseUserInputForUpdateUser(userInput.split(" "));
        LeaderBoardStore store = new LeaderBoardStore();

        store.makePlay(person);
        Assert.assertEquals(1, store.getLeaderBoards().size());
        Assert.assertEquals("Nick", store.getLeaderBoards().getLast().get(0).getName());
    }

    @Test
    public void testUpdatePerson() throws Exception {
        String userInput = "score Nick 10";
        LeaderBoardPerson person = App.parseUserInputForUpdateUser(userInput.split(" "));
        LeaderBoardStore store = new LeaderBoardStore();

        store.makePlay(person);
        Assert.assertEquals(1, store.getLeaderBoards().size());
        Assert.assertEquals("Nick", store.getLeaderBoards().getLast().get(0).getName());

        final int higherScore = 34;
        store.makePlay(LeaderBoardPerson.getInstance("Nick", higherScore));
        Assert.assertEquals(1, store.getLeaderBoards().getLast().size());
        Assert.assertEquals(higherScore, store.getLeaderBoards().getLast().get(0).getScore());
    }
}
