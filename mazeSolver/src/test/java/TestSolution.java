import MazeSolver.SolveMaze;
import org.junit.Assert;
import org.junit.Test;

public class TestSolution {

    //does more than a unit tests should but this isn't real
    @Test
    public void testLoadSimpleMaze() throws Exception {
        var userDir = System.getProperty("user.dir");
        String pathToFile = userDir + "/src/main/resources/input.txt";
        var res = ReadInputFile.parseInputFile(pathToFile);

        Assert.assertEquals(1, res.getStart().getColumn());
    }

    //copy pasted (this has taken enough of my time)
    @Test
    public void testAlg() throws Exception {
        var userDir = System.getProperty("user.dir");
        String pathToFile = userDir + "/src/main/resources/large_input.txt";
        var res = ReadInputFile.parseInputFile(pathToFile);

        var mazeSolver = new SolveMaze();
        boolean ans = mazeSolver.solveMaze(res);
        Assert.assertTrue(ans);
    }

    @Test
    public void testAlgSparse() throws Exception {
        var userDir = System.getProperty("user.dir");
        String pathToFile = userDir + "/src/main/resources/sparse_medium.txt";
        var res = ReadInputFile.parseInputFile(pathToFile);

        var mazeSolver = new SolveMaze();
        boolean ans = mazeSolver.solveMaze(res);
        Assert.assertTrue(ans);
    }

    @Test
    public void testAlgMedium() throws Exception {
        var userDir = System.getProperty("user.dir");
        String pathToFile = userDir + "/src/main/resources/medium_input.txt";
        var res = ReadInputFile.parseInputFile(pathToFile);

        var mazeSolver = new SolveMaze();
        boolean ans = mazeSolver.solveMaze(res);
        Assert.assertTrue(ans);
    }

}
