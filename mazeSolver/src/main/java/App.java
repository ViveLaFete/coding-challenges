import MazeData.MazeMetadata;
import MazeSolver.SolveMaze;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        MazeMetadata mazeMetadata = ReadInputFile.parseInputFile(args[0]);
        SolveMaze solveMaze = new SolveMaze();
        solveMaze.solveMaze(mazeMetadata);
    }
}
