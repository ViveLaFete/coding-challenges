import MazeData.MazeCoordinate;
import MazeData.MazeMetadata;
import MazeData.Pair;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ReadInputFile {

    /**
     * Parses text representation of the mazes
     *
     * @param pathToFile
     * @return MazeMetadata, an in mempory representation of the maze and its start and end
     * @throws IOException
     */
    public static MazeMetadata parseInputFile(String pathToFile) throws IOException {

        List<String> mazeData = new ArrayList<>();
        try (Stream<String> lines = Files.lines(Paths.get(pathToFile), Charset.forName("utf-8"))) {
            lines.forEachOrdered(mazeData::add);
        }
        String[] dimnesions = mazeData.get(0).split(" ");
        String[] start = mazeData.get(1).split(" ");
        String[] end = mazeData.get(2).split(" ");

        Pair sizeArray = Pair.getInstance(dimnesions[0], dimnesions[1]);
        Pair startPosition = Pair.getInstance(start[1], start[1]);
        Pair endPosition = Pair.getInstance(end[1], end[0]);

        return new MazeMetadata(startPosition, endPosition, convertStingToMaze(sizeArray, getFromIndex(3, mazeData)));
    }

    /**
     * converts the 1/0 representation of the maze to an array that can be easily be traversed
     * @param sizeOfMaze
     * @param stringMaze
     * @return the maze
     */
    private static MazeCoordinate[][] convertStingToMaze(Pair sizeOfMaze, List<String> stringMaze) {
        int height = sizeOfMaze.getRow();
        int width = sizeOfMaze.getColumn();

        MazeCoordinate[][] maze = new MazeCoordinate[width][height];
        for (int i = 0; i < width; i++) {
            String thing = stringMaze.get(i);
            String[] row = thing.split(" ");
            for (int j = 0; j < row.length; j++) {
                MazeCoordinate coordinate = new MazeCoordinate();
                if (row[j].equals("0")) {
                    coordinate.setWall(false);
                }
                maze[i][j] = coordinate;
            }
        }
        return maze;
    }

    //dodgy code to rest of the input file. As input is so small unlikely extra pass/memory to have any meaningful
    //impact on runtime. Could be refactored to use a stack to save time
    private static List<String> getFromIndex(int index, List<String> lst) {
        List<String> copyList = new ArrayList<>(lst.size() - index);
        for (int i = index; i< lst.size(); i++) {
            copyList.add(lst.get(i));
        }
        return copyList;
    }

}
