package MazeSolver;

import MazeData.MazeCoordinate;
import MazeData.MazeMetadata;
import MazeData.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SolveMaze {

    //as unchanging ok to have in this scope
    private Pair start;
    private Pair end;

    /**
     * Solves the maze using dfs, prints output to terminal
     *
     * @param mazeMetadata an object containing information on the maze itself, its start and end position
     * @return whether the maze has a solution
     */
    public boolean solveMaze(MazeMetadata mazeMetadata) {
        this.start = mazeMetadata.getStart();
        this.end = mazeMetadata.getEnd();
        var maze = mazeMetadata.getMaze();

        List<Pair> path = new LinkedList<>();
        boolean ans = solveMazeDFS(maze, start, false, path);

        //this basically doesn't work but the principle is follow pointers from the end of the maze to the start
        for (Pair p : path) {
            maze[p.getRow()][p.getColumn()].setPath(true);
        }
        printMaze(maze);
        return ans;
    }

    /**
     * Simple DFS implementation that recursively tries possible paths through the maze until it finds a
     * successful solution.
     * @param maze a 2d array containing inforamtion on whether the node has already been visited and whether
     *             it is impassable
     * @param currentLoc a 'pointer' to where in the maze we currently are
     * @param foundSoln records whether a solution has been found
     * @return true if solution exists
     */
    private boolean solveMazeDFS(MazeCoordinate[][] maze, Pair currentLoc, boolean foundSoln, List<Pair> path) {
        path.add(currentLoc);
        if (currentLoc.equals(end)) {
            return true;
        }
        if (maze[currentLoc.getRow()][currentLoc.getColumn()].isDiscovered()) {
            path.remove(currentLoc);
            return false;
        }
        maze[currentLoc.getRow()][currentLoc.getColumn()].setDiscovered(true);
        for (Pair next : visitableNodes(maze, currentLoc)) {
            foundSoln |= solveMazeDFS(maze, next, foundSoln, path);
        }
        return foundSoln;
    }

    /**
     * finds possible paths from this current node
     */
    private List<Pair> visitableNodes(MazeCoordinate[][] maze, Pair currentLoc) {
        final int maxWidth = maze[0].length;
        final int maxHeight = maze.length;
        final int column = currentLoc.getColumn();
        final int row = currentLoc.getRow();

        List<Pair> adjacentEdges = new ArrayList<>(4);

        if (isInBounds(column + 1, maxWidth) && isNotWallOrDisovered(maze[row][column + 1])) {
            adjacentEdges.add(Pair.getInstance(row, column + 1));
        }
        if (isInBounds(column - 1, maxWidth) && isNotWallOrDisovered(maze[row][column - 1])) {
            adjacentEdges.add(Pair.getInstance(row, column - 1));
        }
        if (isInBounds(row - 1, maxHeight) && isNotWallOrDisovered(maze[row - 1][column])) {
            adjacentEdges.add(Pair.getInstance(row - 1, column));
        }
        if (isInBounds(row + 1, maxHeight) && isNotWallOrDisovered(maze[row + 1][column])) {
            adjacentEdges.add(Pair.getInstance(row + 1, column));
        }

        return adjacentEdges;
    }

    private boolean isNotWallOrDisovered(MazeCoordinate coorrd) {
        return ! (coorrd.isDiscovered() || coorrd.isWall());
    }

    private boolean isInBounds(int position, int bound) {
        return position >= 0 && position <= bound;
    }

    private void printMaze(MazeCoordinate[][] maze) {
        for (var x : maze) {
            for (var y : x) {
                System.out.print(y.toString());
            }
            System.out.println();
        }
    }

}
