package MazeData;

/**
 * Included to make reading operations on the maze easier to follow. If memory must be minismised maze could be
 * refactored to use boolean arrays
 */
public class MazeCoordinate {

    private boolean isWall = true;
    private boolean isDiscovered = false;
    private boolean isPath;

    public boolean isWall() {
        return isWall;
    }

    public void setWall(boolean wall) {
        isWall = wall;
    }

    public boolean isDiscovered() {
        return isDiscovered;
    }

    public void setDiscovered(boolean discovered) {
        isDiscovered = discovered;
    }

    public boolean isPath() {
        return isPath;
    }

    public void setPath(boolean path) {
        isPath = path;
    }

    @Override
    public String toString() {
        if (isWall) {
            return "#";
        } else if (isPath) {
            return "X";
        } else {
            return " ";
        }
    }
}
