package MazeData;

/**
 * Convience class for representing 2 integers
 */
public class Pair {

    private int column;
    private int row;

    private Pair(int row, int col) {
        this.column = col;
        this.row = row;
    }

    public static Pair getInstance(int row, int col) {
        return new Pair(row, col);
    }

    public static Pair getInstance(String row, String col) {
        int rowPos = Integer.parseInt(row);
        int colPos = Integer.parseInt(col);
        return new Pair(rowPos, colPos);
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public boolean equals(Pair pair) {
        return this.getRow() == pair.getRow() && this.getColumn() == pair.getColumn();
    }

    @Override
    public String toString() {
        return "col: " + column + " row: " + row + " ";
    }
}
