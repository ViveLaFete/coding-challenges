package MazeData;

public class MazeMetadata {

    private Pair start;
    private Pair end;
    private MazeCoordinate[][] maze;

    public MazeMetadata(Pair start, Pair end, MazeCoordinate[][] maze) {
        this.start = start;
        this.end = end;
        this.maze = maze;
    }

    public Pair getStart() {
        return start;
    }

    public Pair getEnd() {
        return end;
    }

    public MazeCoordinate[][] getMaze() {
        return maze;
    }

}
