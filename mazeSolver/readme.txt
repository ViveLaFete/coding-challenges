Hi

To run: simple gradle project, can be run through the wrapper or using the jar in ./build/libs. Supply the path to the input maze you want 'solved' e.g. java -jar myJar /path/to/file

Tested using Junit tests, everything is solvable in a short runtime (under the 30s(?!?) time limit)

Written in java10, i hope that will not be a problem